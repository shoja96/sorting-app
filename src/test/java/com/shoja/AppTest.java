package com.shoja;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test(expected = NullPointerException.class)
    public void testNull()
    {
        String[] args=null;
        App.main(args);
    }
    @Test(expected = NumberFormatException.class)
    public void testEmpty()
    {
        String[] args={""};
        App.main(args);
    }
    @Test(expected = NumberFormatException.class)
    public void testNonInteger()
    {
        String[] args={"34 W 45 N"};
        App.main(args);
    }
    @Test
    public void testOutOfBound(){
        PrintStream originalOut = System.out;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
        String[] args={"34", "3", "45", "5", "-2", "0", "7", "88", "19", "0", "11"};
        App.main(args);
        assertEquals("No or more than 10 arguments inserted"+ System.getProperty("line.separator"), bos.toString());
        System.setOut(originalOut);
    }
    @Test
    public void testOne(){
        PrintStream originalOut = System.out;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
        String[] args={"3"};
        App.main(args);
        assertEquals("[3]"+ System.getProperty("line.separator"), bos.toString());
        System.setOut(originalOut);
    }
    @Test
    public void testTen(){
        PrintStream originalOut = System.out;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
        String[] args={"34", "3", "45", "5", "-2", "10", "7", "19", "0", "11"};
        App.main(args);
        assertEquals("[-2, 0, 3, 5, 7, 10, 11, 19, 34, 45]"+ System.getProperty("line.separator"), bos.toString());
        System.setOut(originalOut);
    }
    @Test
    public void testSorted(){
        PrintStream originalOut = System.out;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
        String[] args={"4", "8", "12", "16", "20"};
        App.main(args);
        assertEquals("[4, 8, 12, 16, 20]"+ System.getProperty("line.separator"), bos.toString());
        System.setOut(originalOut);
    }
}
