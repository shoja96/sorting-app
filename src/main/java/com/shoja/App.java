package com.shoja;

import java.util.Arrays;

/**
 * Hello world!
 *
 */
public class App 
{
    /**
     * The main method takes integers as arguments
     * checks if the args are non-integer, empty or more than 10
     * If yes, terminates the app by throwing Exceptions
     * If no, sorts the values in ascending order
     */
    public static void main( String[] args )
    {
        try {
            /** n holds the length of the arguments */
            int n=args.length;
            if(n>=1&&n<=10){
                int[] arr=new int[n];
                for(int i = 0; i<args.length; i++){
                    arr[i]=Integer.parseInt(args[i]);
                }
                /** static method uses Insertion sorting algorithm
                 * to sort the data, and returns String value
                 */
                String res=insertSort(arr);
                System.out.println( res );
            }
            else{
                System.out.println( "No or more than 10 arguments inserted" );
            }
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            throw e;
        }   catch (NullPointerException e){
            System.out.println(e.getMessage());
            throw e;
        }
    }

    private static String insertSort(int[] arr) {
        for (int i = 1; i < arr.length; ++i) {
            int key = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
        return Arrays.toString(arr);
    }
}
